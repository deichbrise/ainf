import java.util.Arrays;
import java.util.Objects;

import AlgoTools.IO;


/**
 * Created by pascalstammer on 22.10.15.
 */
public class Table {
    /**
     *
     */
    protected String[][] table = new String[1][1];

    protected int columnsWidth = 4;

    /**
     *
     * @param row
     * @param column
     * @param value
     */
    public void setValues(int row, int column, String value) {
        // Get the actuall size of the array
        int rows = this.table.length;
        int columns = this.table[0].length;

        if(row > rows - 1 || column > columns - 1) {
            int newRowLength        = (row > rows) ? row : rows,
                newColumnsLength    = (column > columns) ? column : columns;
            String[][] bufferArray = new String[newRowLength + 1][newColumnsLength + 1];

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    bufferArray[i][j] = this.table[i][j];
                }
            }
            this.table = bufferArray;
        }
        this.table[row][column] = value;
    }

    /**
     *
     * @param columnsWidth
     */
    public void setColumnsWidth(int columnsWidth) {
        this.columnsWidth = columnsWidth;
    }

    /**
     *
     * @return the width of the columns
     */
    public int getColumnsWidth () {
        return this.columnsWidth;
    }


    public void print() {
        for (int i = 0; i < this.table.length; i++) {
            for (int j = 0; j < this.table[i].length; j++) {
                String value = this.table[i][j];
                if (value == null) {
                    IO.print("", this.columnsWidth);
                } else {
                    IO.print(this.table[i][j], this.columnsWidth);
                }

            }
            IO.println("");
        }
    }
}
