/******************************  Primzahltest.java  ***************************/

import AlgoTools.IO;

/**
 * @version 28.10.15
 *  
 * @author  chbroecker, pstammer
 */

public class Primzahltestnaiv {

  public static void main(String[] argv) {
     
     int n, i;
     boolean primzahl = true;
     // Ließt eine positive ganze Zahl ein
     do {
         n = IO.readInt("Bitte geben sie eine positive ganze Zahl ein: ");
     }   while (n <= 0 );

     // Testet jeden möglichen Teiler der vermeintlichen Primzahl
     for (i = 2; i < n; i++) {
       if (n % i == 0) 
          primzahl = false;
      
     }     

     // Druckt das Ergebnis
     if(n == 1) IO.println("false");

     else {
         IO.println(primzahl);
     }
  }
}

