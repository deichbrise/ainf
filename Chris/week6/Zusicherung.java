/**************************  Zusicherung.java  *******************************/
import AlgoTools.IO;

/**
 * Beweis der Korrektheit mit Hilfe der Zusicherungsmethode
 */

public class Zusicherung {

  public static void main(String[] args) {

    int i = 0, h = 1, z = 0, n;
    // i = 0 und h = 1 und z = 0 
    do {
      n = IO.readInt("n= ");
    } while (n < 0);
    // i = 0 und h = 1 und z = 0 und n >= 0
    /* z = i * i und h = 2 * i + 1 und i <= n ist Schleifeninvariante */

    while (i < n) { 
    // z = i * i und h = 2 * i + 1 und i <= n und i < n
      z += h;
    // z = i * i + 2 * i + 1 = (i + 1)^2 und h = 2 * i + 1 und i < n
      h += 2;
    // z = (i + 1)^2 und h = 2 * i + 3 und i < n
      i++;
    // z = i^2 und h = 2 * i + 1 und i < n
    }
    // z = i^2 und h = 2 * i + 1 und i <= n
    // z = i^2 und i = n
    // z = n^2 
    IO.println(z, 6);
    //Es wird n^2 ausgegeben.
  }
}
