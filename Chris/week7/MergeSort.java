/******************************  MergeSort.java  ******************************/

import AlgoTools.IO;

/**
 * @version 25.11.15
 *  
 * @author  chbroecker
 */

public class MergeSort {

  // Private Klassenvariable zum zaehlen der Schritte

  private static int schritte = 0;

  /** 
   * Das Array wird rekursiv immer weiter aufgesplittet und mit merge sortiert 
   * @param a Das array was uns gegeben wird
   * @return Sortiertes Array a
   */
  public static int[] sortRekursiv (int[] a) {
    if(a.length == 1){
      return a;
    }
    int n = a.length;
    int[] haelfte1 = new int[n/2];
    int[] haelfte2 = new int[n-haelfte1.length];

    for(int i = 0; i < n; i++){           //Array wird aufgeteilt
      if(i < haelfte1.length){
        haelfte1[i]=a[i];
      } else {
        haelfte2[i-haelfte1.length]=a[i];
      }
    }
    haelfte1 = sortRekursiv(haelfte1);
    haelfte2 = sortRekursiv(haelfte2);
    a = merge(haelfte1, haelfte2);
    return a;

  } 

  /**
   * Fuegt und sortiert 2 Arrays zusammen
   * @param a Verwendetes Arrays
   * @param b Verwendetes Arrays
   * @return sortiertes Array c
   */
  public static int[] merge (int[]a, int[]b) {   // mischt a und b

    int i=0, j=0, k=0;                           // Laufindizes
    int[] c = new int[a.length + b.length];      // Platz fuer Folge c besorgen

    while ((i<a.length) && (j<b.length)) {       // mischen, bis ein Array leer
      if (a[i] < b[j])                           // jeweils das kleinere Element
        c[k++] = a[i++];                         // wird nach c uebernommen
      else
        c[k++] = b[j++];
    }

    while (i<a.length) c[k++] = a[i++];          // ggf.: Rest von Folge a
    while (j<b.length) c[k++] = b[j++];          // ggf.: Rest von Folge b
    schritte++;
    return c;                                    // Ergebnis abliefern
  }

  /**
   * Liest unsortierte Werte ein und gibt sie sortiert wieder aus
   * @param argv
   */
  public static void main(String[] argv) {
    int[] a;

    a = IO.readInts("Bitte geben sie eine Zahlenfolge ein: ");

    if(a.length == 1){
      IO.println(a[0]);
    } else {
      a = sortRekursiv(a);
      IO.println("So sieht ihre Zahlenfolge sortiert aus.");
      for(int i = 0; i < a.length; i++) {
        IO.print(a[i], 2);
      }
      IO.println();
    }
    IO.println("Die Anzahl der Merge-Operationen betrug: " + schritte);
  }
}

