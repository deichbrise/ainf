import AlgoTools.IO;

/**
 * @author pstammer
 * @version 26.10.2015
 *
 * Dieses Programm berechnet die Quersumme einer Zahl
 */
public class Quersumme {

    public static void main(String[] args) {
        int zahl = 0,
            summe = 0;

        // Liest eine Zahl ein, die größer als 0 sein muss
        do {
            zahl = IO.readInt("Bitte geben Sie eine Zahl ein: ");
        } while(zahl < 1);

        /**
         * @var int originalZahl
         *
         * Speichert die Variable Zahl in einer neuen Zahl, da zahl verändert wird und
         * trotzdem die ursprüngliche Zahl ausgegeben werden soll
         */
        int originalZahl = zahl;

        /**
         * Summiert immer die letze Ziffern auf,
         * solange die zahl ungleich 0 ist und entfernt die letzte Ziffer.
         */
        while (zahl != 0) {
            summe += zahl % 10;
            zahl /= 10;
        }

        // Gibt die Zahl aus von der die Quersumme berechnet wurde und die zugehörige Quersumme
        IO.println("Die Quersumme von " + originalZahl + " ist: " + summe );
    }
}