import AlgoTools.IO;
/**
 * @author Pascal Stammer <stammer@deichbrise.de>
 * @Class Zaehler2
 *
 * Diese Klasse berechnet den Modulo von x < k und y < k mit x % y
 * und gibt die Werte in der Konsole aus
 */
public class Zaehler2 {

    public static void main(String[] args) {

        int k;                                                                  //Initialisiere Variable k

        do {
            IO.readInt("Bitte geben Sie eine Zahl zwischen 1 und 15 ein: ");    // Fordere den Nutzer nach einer Zahl
        } while (k < 1 && k > 15)

	    Table table = new Table();                                              // Instanziere eine neue Tabelle
        for (int i = 1; i <= k; i++) {
            for (int j = 1; j <= k ; j++) {
                table.setValues(i, j, i % j + "");                              // Schreibe die Werte in die Tabelle
            }
        }
        table.print();                                                          // Gebe die Tabelle aus

    }
}
