import AlgoTools.IO;

/**
 * @author pstammer, chbroecker
 * @version 09.11.15.
 */
public class Fließkommaautomat {

    public static void main(String[] args) {


        /**
         * @var final byte V
         * @var final byte Z
         * @var final byte K
         */
        final byte V = 0;
        final byte Z = 1;
        final byte K = 2;
        /**
         * @var byte[][] delta
         *
         * Stellt die Überführungsfunktion da:
         * delta[aktueller Zustand][eingabeaplpabet] = nächster Zustand
         */
        byte[][] delta = {
        /* Start */   {1, 2, 5},
        /* +/-   */   {5, 2, 5},
        /* VK    */   {5, 2, 3},
        /* K     */   {5, 4, 5},
        /* NK    */   {5, 4, 5},
        /* F     */   {5, 5, 5}
        };

        char[] eingabe;
        int[] eingabeAlphabet = new int[0];
        boolean korrekteEingabe = true;
        int s = 0;


        /**
         * Liest eine Ziffernfolge mit readChars ein und bestimmt für jede
         * eingegeben Ziffer die zugehörige Zahl, damit nachher einfacher
         * die Überführungsfunktion angewand werden kann.
         */
        do {
            eingabe = IO.readChars("Bitte geben Sie eine Folge von " +
                    "Zeichen an: ");

            if(eingabe.length > 0) {
                eingabeAlphabet = new int[eingabe.length];
                for(int i = 0; i < eingabe.length; i++) {
                    if(korrekteEingabe) {
                        switch(eingabe[i]) {
                            case 'v':case 'V':
                                korrekteEingabe = true;
                                eingabeAlphabet[i] = V;
                                break;
                            case 'z':case 'Z':
                                korrekteEingabe = true;
                                eingabeAlphabet[i] = Z;
                                break;
                            case 'k': case 'K':
                                korrekteEingabe = true;
                                eingabeAlphabet[i] = K;
                                break;
                            default:
                                korrekteEingabe = false;
                                break;
                        }
                    }

                }
            }

        } while(eingabe.length < 1 && !korrekteEingabe);


        /**
         * Wendet die Überführungsfunktion auf alle Zeichen der Zeichenkette
         * an
         */
        for(int i = 0; i < eingabeAlphabet.length; i++) {
            s = delta[s][eingabeAlphabet[i]];
        }

        /**
         * Schaut ob ein korrekter Endzustand erreicht wurde.
         */
        if(s == 4 || s == 2) IO.println("Korrekte Eingabe");
        else IO.println("Falsche Eingabe");


    }
}
