import AlgoTools.IO;

/**
 * @author pstammer, chbroecker
 * @version 08.11.15.
 *
 * Dieses Programm berechnet das Marizenprodut zwei eingegebener Matrizen
 */
public class Matrizenmultiplikation {

    public static void main(String[] args) {

        /**
         * @var int zeilen
         * @var int spalten
         * @var int productBuffer
         */
        int     zeilen,
                spalten,
                produktBuffer = 0;

        /**
         * liest Zeilen- und Spaltenanzahl ein
         */
        do {
            zeilen = IO.readInt("Bitte geben Sie die Anzahl an Zeilen " +
                    "für die erste Matrize an: ");
        } while(zeilen < 1);
        do {
            spalten = IO.readInt("Bitte geben Sie die Anzahl an Spalten " +
                    "für die erste Matrize an: ");
        } while(spalten < 1);


        /**
         * @var int[][] matrix1
         * @var int[][] matrix2
         * @var int[] buffer
         */
        int[][] matrix1 = new int[zeilen][spalten],
                matrix2 = new int[spalten][zeilen];
        int[] buffer;

        /**
         * liest die Matritzen Spaltenweise ein
         */
        for(int i = 0; i < zeilen; i++) {
            do {
                buffer = IO.readInts("Bitte geben Sie die " + (i + 1) +
                        " Zeile der Matrize an: ");
            } while(buffer.length != matrix1[i].length);
            matrix1[i] = buffer;
        }

        for(int i = 0; i < spalten; i++) {
            do {
                buffer = IO.readInts("Bitte geben Sie die " + (i + 1) +
                        " Zeile der Matrize an: ");
            } while(buffer.length != matrix2[i].length);
            matrix2[i] = buffer;
        }

        /**
         * geht jede Zeile für die erste Matrix ein,
         * dann multiplziere und summiere die Zeilenskalare
         * der erste Matrix mit den Skalaren der zugehörigen Spalte des zweiten
         * Matrix
         */
        for(int i = 0; i < matrix1.length; i++) {
            for(int k = 0; k < matrix2[0].length; k++) {
                for(int j = 0; j < matrix2.length; j++) {
                    produktBuffer += matrix1[i][j] * matrix2[j][k];
                }
                IO.print(produktBuffer, 4);
                produktBuffer = 0;
            }
            IO.println();
        }
    }
}
