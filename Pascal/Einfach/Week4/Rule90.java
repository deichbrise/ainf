import AlgoTools.IO;

/**
 * @author chbroecker, pstammer
 * @version 05.11.15.
 */
public class Rule90 {

    public static void main(String[] args) {

        /**
         * @var int[] start
         *
         * Ließt ein Array an Zahlenwerten ein
         */
        int[] start;
        do {
            start = IO.readInts("Bitte geben Sie eine Zahlenfolge ein " +
                    "getrennt mit einem Leerzeichen: ");
        } while(start.length == 0);

        /**
         * @var int anzahl
         * @var boolean[] vorher
         * @var boolean[] jetzt
         *
         * die Größen der Arrays werden dynmaisch durch die Startwerte bestimmt
         */
        int anzahl;
        boolean[] vorher = new boolean[start.length];
        boolean[] jetzt = new boolean[start.length];

        do {
            anzahl = IO.readInt("Bitte geben Sie die Anzahl der Zeitschritte" +
                    " an:");
        } while(anzahl < 1);

        /**
         * Ints zu booleans casten
         */
        for(int i = 0; i < start.length; i++) {
            vorher[i] = (start[i] == 0) ? false : true;
            jetzt[i] = (start[i] == 0) ? false : true;

        }


        /**
         * gibt den aktuellen Wert, der in jetzt gespeichert ist aus
         * '@' wenn die Population lebt, '.' für tot
         *
         * Die Werte für den Zeitschritt berechnen sich dertimistisch aus den
         * vorherigen Werten, sodass die Population vorher genau einen
         * Nachbarn hat, der den Wert 'lebend' besitzt. Die äußeren Populationen
         * werden jeweils miteinander verbunden
         */
        for(int j = 0; j < anzahl; j++) {
            for(int i = 0; i < start.length; i++) {
                if(jetzt[i]) {
                    IO.print("@", 2);
                } else if (!jetzt[i]) {
                    IO.print(".", 2);
                }
            }
            for ( int i = 0; i < jetzt.length; i++) {
                if(i == 0) {
                    jetzt[i] = (vorher[vorher.length - 1] ^ vorher[i + 1]);
                } else if (i == jetzt.length - 1) {
                    jetzt[i] = (vorher[i - 1] ^ vorher[0]);
                } else {
                    jetzt[i] = (vorher[i - 1] ^ vorher[i + 1]);
                }
            }

            for(int i = 0; i < jetzt.length; i++) {
                vorher[i] = jetzt[i];
            }
            IO.println();
        }
    }
}
