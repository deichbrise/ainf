

/**
 * Implemntiert einen VerweisKeller
 *
 * @author pstammer, chbroecker
 * @version 16.12.15.
 */
public class KellerListe implements Liste {

    protected Keller x;
    protected Keller y;

    public KellerListe() {
        x = new VerweisKeller();
        y = new VerweisKeller();
    }

    /**
     * Prüft ob die KellerListe leer ist
     * @return ist leer
     */
    public boolean empty() {
        return x.empty() && y.empty();
    }

    /**
     * Schaut ob der Zeiger auf die Endposition zeigt
     * @return
     */
    public boolean endpos() {
        return x.empty();
    }

    /**
     * Setzt den Zeiger auf den Anfang
     */
    public void reset() {
        while(!y.empty()) {
            x.push(y.top());
            y.pop();
        }
    }

    /**
     * Rückt ein Element vor
     */
    public void advance() {
        if(endpos()) throw new RuntimeException("Das Ende der Liste ist " +
                "erreicht");

        y.push(x.top());
        x.pop();
    }

    /**
     * Liefert das aktuelle Element
     * @return aktuelles Element
     */
    public Object elem(){
        return x.top();
    }

    /**
     * Fügt ein Elemen an der aktuellen Stelle ein
     * @param newO
     */
    public void insert(Object newO){
        x.push(newO);
    }

    /**
     * Löscht das Element an der aktuellen Stelle
     *
     */
    public void delete(){
        x.pop();
    }
}
