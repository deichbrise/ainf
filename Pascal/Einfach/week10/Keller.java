

/**
 * @author pascalstammer
 * @version 16.12.15.
 */
public interface Keller {

    public boolean empty(); // liefert true, falls Keller leer, false sonst

    public void push(Object x); // legt Objekt x auf den Keller

    public Object top(); // liefert oberstes Kellerelement

    public void pop(); // entfernt oberstes Kellerelement

}
