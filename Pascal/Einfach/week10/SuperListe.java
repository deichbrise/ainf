

/**
 * Implemetiert eine SuperListe
 * @author pstammer, chbrocker
 * @version 16.12.15.
 */
public class SuperListe extends VerweisListe {

    /**
     * Dreht die Liste um indem zuerst die Elemente in eine Verweisschlange
     * kopiert werden und danach Rückwärts in die aktuelle Liste sortiert werden
     */
    public void umdrehen() {

        VerweisSchlange s = new VerweisSchlange();
        reset();

        while(!empty()) {
            s.enq(elem());
            delete();
        }

        while(!s.empty()) {
            insert(s.front());
            s.deq();
        }
    }

    /**
     * Löscht alle Duplikate aus der Liste
     * Sortiert alle nicht doppelten Elemente in eine neue Liste, löscht jedes
     * Element aus der aktuellen Liste und schreibt dann alle Elemente aus
     * der Übergangsliste, in der kein doppeltes Element mehr vorhanden ist,
     * zurück in die originale Liste
     */
    public void unique() {
        boolean notUnique = false;
        reset();
        Liste hilf = new VerweisListe();
        Object temp;

        while( !endpos() ){

            temp = elem();

            while( !hilf.endpos() && !notUnique ){
                notUnique = temp.equals( hilf.elem() );
                hilf.advance();
            }

            hilf.reset();

            if( !notUnique )
                hilf.insert(temp);

            delete();
            notUnique = false;
        }

        while( !hilf.endpos() ){
            insert( hilf.elem() );
            hilf.delete();
        }
    }

    /**
     * Liefert das Element an der Stelle n zurück
     *
     * @param n der Index des zu liefernden Elements
     * @return das Element mit dem Index n
     * @throws RuntimeException falls die Liste kleiner als n ist
     */
    public Object elem(int n) {
        if(n < 0)
            throw new RuntimeException("n muss > 0 sein.");
        reset();
        for(int i=1; i<n; i++){
            if(endpos())
                throw new RuntimeException("Kein Element an der Stelle n");
            advance(); }
        return elem();
    }
}
