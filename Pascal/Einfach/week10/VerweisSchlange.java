

/**
 * @author pstammer, chbroecker
 * @version 03.01.16.
 */
public class VerweisSchlange implements Schlange {
    /**
     * Verweis auf das Schlangenende
     */
    private Eintrag tail;

    /**
     * Verweis auf den Schlangenkopf
     */
    private Eintrag head;

    /**
     * Contructor
     */
    public VerweisSchlange() {
        head = tail = null;
    }

    /**
     * Prüft, ob Schlange leer
     * @return Schlange leer
     */
    public boolean empty() {
        return head == null;
    }

    /**
     * Fügt Element an das hintere Ende der Schlange an
     * @param x
     */
    public void enq(Object x) {
        if (empty())
            tail = head = new Eintrag();
        else {
            tail.next = new Eintrag();
            tail = tail.next;
        }
        tail.inhalt = x;
        tail.next = null;
    }

    /**
     * Liefert das erste Elemen zurück
     * @return
     */
    public Object front(){
        if (empty())
            throw new RuntimeException("front: Schlange ist leer");
        return head.inhalt;
    }

    /**
     * Entfernt das erste Element
     */
    public void deq() {
        if (empty())
            throw new RuntimeException("deq: Schlange ist leer");
        head = head.next;
        if (head == null)
            tail = null;
    }
}
