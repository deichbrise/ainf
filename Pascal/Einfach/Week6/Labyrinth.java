/******************************  Labyrinth.java  ******************************/

/**
 * Liest ein Labyrinth ein und berechnet mittels einem Backtracking
 * Algorithmus einen Weg durch dieses.
 *
 * @author Christian Heiden
 * @author Lukas Kalbertodt
 */

import AlgoTools.IO;

public class Labyrinth {

	/** Freies Feld */
	private static final char FREI  = ' ';
	/** Mauer */
	private static final char MAUER = '#';
	/** Startfeld */
	private static final char START = 'S';
	/** Zielfeld */
	private static final char ZIEL  = 'Z';
	/** Besuchtes Feld */
	private static final char PFAD  = '-';


	/**
	 * Findet einen Weg durch das Labyrinth.
	 * Findet den Startpunkt und ruft findeZielRek mit dem Startpunkt auf.
	 *
	 * @param lab Das Labyrinth
	 * @throws RuntimeException Wenn kein Startpunkt gefunden wurde
	 * @return Ob ein Weg gefunden wurde
	 */
	private static boolean findeZiel(char[][] lab) {

		int startX = 0;
		int startY = 0;
		boolean foundStart = false;

		// Sucht das Startfeld und setzt die entsprechenden Startwerte
		for(int i = 0; i < lab.length && !foundStart; i++) {
			for(int j = 0; j < lab[i].length && !foundStart; j++) {
				if(lab[i][j] == START) {
					foundStart = true;
					startY = i;
					startX = j;
				}
			}
		}
		if(foundStart) {
			// Entferne das Startzeichen und starte die Rekursion
			lab[startY][startX] = FREI;
			findeZielRek(lab, startX, startY);
		} else {
			// Wenn kein Startwert gefunden wurde
			throw new RuntimeException("Es wurde kein Startpunkt gefunden.");
		}

		// Fuegt das Startzeichen wieder hinzu
		lab[startY][startX] = START;
		return true;
	}


	/**
	 * Findet rekursiv einen Weg durch das Labyrinth
	 *
	 * @param lab Das Labyrinth
	 * @param x X-Koordinate des Punktes bei dem man steht
	 * @param y Y-Koordinate des Punktes bei dem man steht
	 * @return Ob ein Weg gefunden wurde
	 */
	private static boolean findeZielRek(char[][] lab, int x, int y) {

		if(lab[y][x] == ZIEL) {
			// Ziel gefunden
			return true;
		} else if (lab[y][x] == MAUER) {
			// Kein Weg, da Mauer im Weg
			return false;
		} else if (lab[y][x] == PFAD) {
			// Schon vorher gelaufen, wuerde keinen Sinn machen, hier weiter
			// zu gehen
			return false;
		} else {

			// Setzt einen Pfad
			if(lab[y][x] == FREI) {
				lab[y][x] = '-';
			}

			if(findeZielRek(lab, x, y - 1)) {
				// testet die obere Umgebung
				return true;
			} else if(findeZielRek(lab, x, y + 1)) {
				// testet die untere Umgebung
				return true;
			} else if(findeZielRek(lab, x - 1, y)) {
				// testet die linke Umgebung
				return true;
			} else if(findeZielRek(lab, x + 1, y)) {
				// testet die rechte Umgebung
				return true;
			}

			// Ist kein Weg, dann entferne das Wegzeichen
			lab[y][x] = FREI;
			return false;
		}
	}

	/**
	 * Zeigt das uebergebene Labyrinth auf dem Terminal an
	 *
	 * @param lab Das anzuzeigende Labyrinth
	 */
	private static void druckeLabyrinth(char[][] lab) {
		for(int i = 0; i < lab.length; i++) {
			for(int j = 0; j < lab[i].length; j++) {
				IO.print(lab[i][j]);
			}
			IO.println();
		}
	}


	/**
	 * Liest das Labyrinth vom Terminal ein
	 * @return Das eingelesene Labyrinth
	 */
	private static char[][] leseEingabe() {
		int breite = 0, hoehe = 0;

		// Lese Breite ein
		do {
			breite = IO.readInt("Breite des Labyrinths: ");
		} while(breite <= 0);

		// Lese Hoehe ein
		do {
			hoehe = IO.readInt("Hoehe des Labyrinths: ");
		} while(hoehe <= 0);

		// Lege Array an, welches ausgegeben wird
		char[][] out = new char[hoehe][breite];

		// Fuer jede Zeile
		for(int i = 0; i < hoehe; i++) {
			char[] zeile;

			// lies Zeile mit korrekter Anzahl von Buchstaben ein
			do {
				zeile = IO.readChars("Zeile " + i + " des Labyrinths: ");
			} while(zeile.length != breite);

			out[i] = zeile;
		}
		return out;
	}

	/**
	 * Liest Labyrinth ein und findet einen Weg von S nach Z
	 */
	public static void main(String[]args){

		// Lese Labyrinth ein
		char[][] lab = leseEingabe();

		// Gebe aus
		IO.println();
		IO.println("----- Original-Labyrinth: -----");
		druckeLabyrinth(lab);
		IO.println();

		boolean gefunden = findeZiel(lab);

		if(gefunden) {
			// Gebe aus
			IO.println("-----   Gefundener Weg:   -----");
			druckeLabyrinth(lab);
		} else {
			IO.println("Kein Weg zum Ziel gefunden!");
		}
	}
}
