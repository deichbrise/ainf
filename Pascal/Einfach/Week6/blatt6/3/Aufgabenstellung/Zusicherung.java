/**************************  Zusicherung.java  *******************************/
import AlgoTools.IO;

/**
 * Beweis der Korrektheit mit Hilfe der Zusicherungsmethode
 */

public class Zusicherung {

  public static void main(String[] args) {

    int i = 0, h = 1, z = 0, n;

    do {
      n = IO.readInt("n= ");
    } while (n < 0);

    /* z = i * i und h = 2 * i + 1 und i <= n ist Schleifeninvariante */

    while (i < n) {

      z += h;

      h += 2;

      i++;
    }

    IO.println(z, 6);
  }
}
