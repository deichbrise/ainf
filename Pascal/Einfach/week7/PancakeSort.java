import AlgoTools.IO;

public class PancakeSort {
    /**
     * Dreht die Reihenfolge der ersten <tt>count</tt> Element in
     * <tt>array</tt> um.
     *
     * @param array das zu sortierende Array
     * @param count Anzahl zu flippender Elemente
     * @throws RuntimeException wenn <tt>count</tt> > <tt>array.length</tt>
     */
    public static void flip(int[] array, int count) {
        if(count > array.length) throw new RuntimeException("Count ist größer " +
                "als die Array Länge");

        // Vertauscht bis zur Häfte von Count vorne und hinten
        for (int i = 0; i < (count+1) / 2; ++i) {
            int tmp = array[i];
            array[i] = array[count-i];
            array[count-i] = tmp;
        }
    }

    /**
     * Gibt ein Array auf dem Terminal aus.
     * <p>
     * Beispiel: Ein Array mit den Zahlen 1, 2 und 3 als Inhalt
     * wird ausgegeben als: 1 2 3
     *
     * @param array Das auszugebene Array
     */
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            IO.print(array[i], 4);
        }
        IO.println();
    }

    /**
     * Sortiert das gegebene <tt>array</tt> mit dem PancakeSort Verfahren.
     *
     * @param array zu sortierendes Array
     */
    public static void sort(int[] array) {
        int     max,
                count;

        for(int i = array.length; i > 0; i--) {
            max = 0;
            count = 0;

            // Suche Maximum
            for(int j = 0; j < i; j++) {
                if(array[j] > max) {
                    max = array[j];
                    count = j;
                }
            }

            // Flip array
            flip(array, count);
            flip(array, i-1);
        }
        printArray(array);
    }

    public static void main(String[] args) {
        int[] array = IO.readInts("Bitte geben Sie eine durch Leerzeichen" +
                "getrennte Zahlenfolge zum Sortieren ein: ");

        // Startet den Sortieralgorithmus
        sort(array);
    }
}
