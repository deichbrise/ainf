import AlgoTools.IO;

/**
 * @author pstammer, chbroecker
 * @version 29.11.15.
 */
public class MergeSort {
    public static void main(String[] args) {
        int[] array = IO.readInts("Bitte geben Sie eine durch Leerzeichen" +
                "getrennte Zahlenfolge an: ");

        // Startet das sortieren
        int[] sort = mergeSort(array);

        // Druckt das Array ins Terminal
        printArray(sort);
    }

    /**
     * Sortiert rekursiv mit dem MergeSort Algoritmus das Array
     * @param liste
     * @return das sortierte Array
     */
    private static int[] mergeSort(int[] liste) {
        int mitte = liste.length / 2;
        int[]   linkeListe  = new int[mitte],
                rechteListe = new int[liste.length - mitte];

        if(liste.length <= 1) {
            return liste;
        } else {
            for(int i = 0; i < liste.length; i++) {
                if(i < mitte) {
                    linkeListe[i] = liste[i];
                } else {
                    rechteListe[i - mitte] = liste[i];
                }
            }
            return Merge.merge(mergeSort(linkeListe), mergeSort(rechteListe));
        }
    }

    /**
     * Gibt ein Array auf dem Terminal aus.
     * <p>
     * Beispiel: Ein Array mit den Zahlen 1, 2 und 3 als Inhalt
     * wird ausgegeben als: 1 2 3
     *
     * @param array Das auszugebene Array
     */
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            IO.print(array[i], 4);
        }
        IO.println();
    }
}
