/**
 * Diese Klasse stellt eine Kugel da
 *
 * @author pascalstammer
 * @version 09.12.15.
 */
public class Sphere extends Shape {

    /**
     * Radius der Kugel
     */
    private double radius;

    public Sphere() {
        super("schwarz", 0.0, 0.0, 0.0);
        setRadius(1.0);
    }
    /**
     * Kontruktor für Sphere
     * @param colour Farbe der Kugel
     * @param x x-Koordinate
     * @param y y-Koordinate
     * @param z z-Koordinate
     * @param radius Radius der Kugel
     */
    public Sphere(String colour, double x, double y, double z, double radius) {
        super(colour, x, y, z);
        setRadius(radius);
    }

    /**
     * Berechnet das Volumen der Kugel
     * @return das Volumen der Kugel
     */
    @Override public double getVolume() {
        double volume = 4 / 3 * (Math.PI * this.radius * this.radius *
                this.radius);

        return volume;
    }

    /**
     * Berechnet die Oberfläche der Kugel
     * @return die Oberfläche der Kugel
     */
    @Override public double getArea() {
        double area = 4 * Math.PI * this.radius * this.radius;

        return area;
    }

    @Override public String toString() {
        return "Color: " + this.colour + "X: " + this.x + ", Y: " + this.y + ", Z: " + this.z + ", " +
                "Radius: " + this.radius;
    }

    @Override public double getDistanceTo(Shape other) {
        double abstand = super.getDistanceTo(other);

        if(other instanceof Sphere) {
            abstand -= 2 * radius;
        } else {
            abstand -= radius;
        }
        return abstand;
    }
    /**
     *
     * @return den Radius der Kugel
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * Setter für den Radius der Kugel
     * @param radius neuer Radius
     */
    public void setRadius(double radius) {
        if( radius < 0 ) throw new RuntimeException( "Der Radius der Kugel" +
                "darf nicht kleiner als 0 sein." );
        this.radius = radius;
    }



}
