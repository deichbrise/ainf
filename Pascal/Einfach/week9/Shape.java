/**
 * Diese Klasse stellt eine allgemeine Form da
 *
 * @author pascalstammer
 * @version 09.12.15.
 */
public class Shape {

    /**
     * Farbe der Form
     */
    protected String colour;

    /**
     * x-, y-, z-Koordinate
     */
    protected double x, y, z;

    /**
     * Default Konstruktor der Klasse Shape
     */
    public Shape() {
        setColour("schwarz");
        setX(0.0);
        setY(0.0);
        setZ(0.0);
    }

    /**
     * Konstruktor der Form-Klasse
     * @param colour Farbe der Form
     * @param x x-Koordinate
     * @param y y-Koordinate
     * @param z z-Koordinate
     */
    public Shape(String colour, double x, double y, double z) {
        setColour(colour);
        setX(x);
        setY(y);
        setZ(z);
    }

    /**
     * Verschieb die Form um dX, dY und dZ
     *
     * @param dX Wert um den die Form in x-Richtung verschoben werden soll
     * @param dY Wert um den die Form in y-Richtung verschoben werden soll
     * @param dZ Wert um den die Form in z-Richtung verschoben werden soll
     */
    public void move(double dX, double dY, double dZ) {
        this.x += dX;
        this.y += dY;
        this.z += dZ;
    }

    /**
     * Berechnet das Volumen der Form
     *
     * @return das Volumen der Form
     */
    public double getVolume() {
       return 0.0;
    }

    /**
     * Berechnet die Oberfläche
     *
     * @return die Oberfläche der Form
     */
    public double getArea() {
        return 0.0;
    }

    /**
     * Berechnet den Abstand zwischen zwei Formen nach der euklidischen Metrik
     *
     * @param other
     * @return
     */
    public double getDistanceTo(Shape other) {
        double  oX = other.getX(),
                oY = other.getY(),
                oZ = other.getZ();

        double  dX = oX - this.x,
                dY = oY - this.y,
                dZ = oZ - this.z;

        double abstand = Math.sqrt((dX * dX) + (dY * dY) + (dZ * dZ));

        return abstand;

    }

    /**
     * Wandelt alle Attribute der Klasse in einen String um.
     *
     * @return die Attribute als String
     */
    public String toString() {
       return "Color: " + this.colour + "X: " + this.x + ", Y: " + this.y + ", Z: " + this.z;
    }

    /**
     * Getter für Farbe
     *
     * @return Farbe der Form
     */
    public String getColour() {
        return this.colour;
    }

    /**
     * Setter für Farbe
     * @param colour neue Farbe
     */
    public void setColour(String colour) {
        this.colour = colour;
    }

    /**
     * Getter für x-Position
     * @return x-Koordinate der Form
     */
    public double getX() {
        return this.x;
    }

    /**
     * Setter für die x-Koordinate
     * @param x neue x-Koordinate
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Getter für y-Position
     * @return y-Koordinate der Form
     */
    public double getY() {
        return this.y;
    }

    /**
     * Setter für die y-Koordinate
     * @param y neue y-Koordinate
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Getter für z-Position
     * @return z-Koordinate der Form
     */
    public double getZ() {
        return this.z;
    }

    /**
     * Setter für die z-Koordinate
     * @param z neue z-Koordinate
     */
    public void setZ(double z) {
        this.z = z;
    }
}
