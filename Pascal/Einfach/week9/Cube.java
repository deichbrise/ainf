/**
 * Diese Klasse stellt einen Würfel da
 *
 * @author pascalstammer
 * @version 09.12.15.
 */
public class Cube extends Shape {

    /**
     * Breite des Würfels
     */
    private double width;

    public Cube() {
        super( "schwarz", 0.0, 0.0, 0.0 );
        setWidth(1.0);
    }
    /**
     * Konstruktor des Würfels
     * @param colour Farbe
     * @param x x-Koordinate
     * @param y y-Koordinate
     * @param z z-Koordinate
     * @param width Breite
     */
    public Cube(String colour, double x, double y, double z, double width) {
        super(colour, x, y, z);
        setWidth(width);
    }

    /**
     * Berechnet das Volumen des Würfels
     * @return das Volumen das Würfels
     */
    @Override public double getVolume() {
        double volumen = this.x * this.y * this.z;
        return volumen;
    }

    /**
     * Berechnet die Oberfläche des Würfels
     * @return die Oberfläche des Würfels
     */
   @Override public double getArea() {
        double area = 6 * this.width * this.width;
        return area;
    }


    /**
     * Berechnet die Distanz zwischen zwei Figuren
     * @param other andere Figur
     * @return die Distanz
     */
    @Override public double getDistanceTo(Shape other) {
        double  oX = other.getX(),
                oY = other.getY(),
                oZ = other.getZ(),
                dX, dY, dZ;

        if(other instanceof Cube) {
            oX -= ((Cube) other).getWidth() / 2;
            oY -= ((Cube) other).getWidth() / 2;
            oZ -= ((Cube) other).getWidth() / 2;
            dX = oX - (this.x - width / 2);
            dY = oY - (this.y - width / 2);
            dZ = oZ - (this.z - width / 2);
        } else {
            dX = oX - this.x;
            dY = oY - this.y;
            dZ = oZ - this.z;
        }

        double abstand = Math.sqrt((dX * dX) + (dY * dY) + (dZ * dZ));
        return abstand;
    }

    /**
     * Setter Methode für die Breite
     * @param width Breite des Wuerfels
     */
    public void setWidth(double width) {
        if( width < 0) throw new RuntimeException("Die Breite des Würfels " +
                "darf nicht kleiner als 0 sein");
        this.width = width;
    }

    /**
     * Getter Methode für die Breite
     * @return die Breite des Wuerfels
     */
    public double getWidth() {
        return width;
    }

    public String toString() {
        return "Color: " + this.colour + "X: " + this.x + ", Y: " + this.y + ", Z: " + this.z + ", " +
                "Width: " + this.width;
    }
}
