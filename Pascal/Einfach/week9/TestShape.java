import AlgoTools.IO;

/**
 * Programmbeschreibung: Testet die Klassen Shape, Sphere und Cube.
 */
public class TestShape {
    public static void main(String[] argv) {

        // Demo Objects
        Shape shape = new Shape();
        Sphere sphere1 = new Sphere();
        Sphere sphere2 = new Sphere("blau", 2, 2, 1, 2);
        Cube cube1 = new Cube("rot", 0, 1, 2, 3);
        Cube cube2 = new Cube("gelb", 1, 0, 2, 1.5);

        // Test der Klasse Shape
        testShape(shape);

        // Test der Klasse Sphere
        testSphere(sphere1, sphere2, shape);

        // Test der Klasse Cube
        testCube(cube1, cube2, sphere2);
    }

    /**
     * Testet die Klasse Shape
     * @param shape Testkörper
     */
    public static void testShape(Shape shape) {

        IO.println("Shape shape: " + shape);
        shape.setColour("gruen");
        shape.move(2.0, 3.0, 1.0);
        IO.println("Shape shape: " + shape + ", Flaeche=" + shape.getArea()
                + ", Volumen=" + shape.getVolume());
        IO.println("Abstand zum Ursprung=" + shape.getDistanceTo(new Shape()));
        shape.setX(3);
        shape.setY(0);
        shape.setZ(2);
        IO.println("Abstand zum Ursprung=" + shape.getDistanceTo(new Shape()));
    }

    /**
     * Testet die Klasse Sphere
     * @param sphere1 Erste Vergleichskugel
     * @param sphere2 Zweite Vergleichskugel
     * @param shape Vergleichspunkt
     */
    public static void testSphere(Sphere sphere1, Sphere sphere2, Shape shape) {

        IO.println("Sphere sphere1: " + sphere1);
        sphere1.setRadius(2);
        IO.println("Neuer Radius=" + sphere1.getRadius());
        IO.println("Sphere sphere1: " + sphere1);

        IO.println("Sphere sphere2: " + sphere2);
        IO.println("Abstand zum Ursprung=" + sphere2.getDistanceTo(new Shape()));
        IO.println("Abstand zu shape=" + sphere2.getDistanceTo(shape));
        IO.println("Abstand zum Einheitskreis im Ursprung="
                + sphere2.getDistanceTo(new Sphere()));
        IO.println("Flaeche=" + sphere2.getArea() + "; Volumen="
                + sphere2.getVolume());
    }

    /**
     * Testet die Klasse Cube
     * @param cube1
     *              Erster Vergleichswürfel
     * @param cube2
     *              Zweiter Vergleichswürfel
     * @param sphere Vergleichskugel
     */
    public static void testCube(Cube cube1, Cube cube2, Sphere sphere) {

        IO.println("Cube cube1:" + cube1);
        IO.println("Flaeche=" + cube1.getArea() + "; Volumen="
                + cube1.getVolume());
        cube1.move(-2, -3, -1);
        cube1.setWidth(5);
        IO.println("Neue Breite=" + cube1.getWidth());
        IO.println("Cube cube1:" + cube1);
        IO.println("Flaeche=" + cube1.getArea() + ", Volumen="
                + cube1.getVolume());
        IO.println("Abstand zum Ursprung=" + cube1.getDistanceTo(new Shape()));
        IO.println("Abstand von cube1 zu sphere2="
                + cube1.getDistanceTo(sphere));
        IO.println("Abstand von sphere2 zu cube1="
                + sphere.getDistanceTo(cube1));

        IO.println("Cube cube2:" + cube2);
        IO.println("Abstand von cube1 zu cube2="
                + cube1.getDistanceTo(cube2));
        IO.println("Abstand von cube2 zu cube1="
                + cube2.getDistanceTo(cube1));
    }
}
