import AlgoTools.IO;

/**
 * @author pstammer, cbroecker
 *
 * @version 26.10.2015
 *
 *  Diese Klasse druckt in einer ANSII Tabelle
 *  die Berechneten Werte für jedes y < einer
 *  Zahl und x < einer Zahl den Modulo.
 */
public class Tabelle {

    public static void main (String [] args) {
        int n;

        /**
         * liest eine Zahl n ein, die größer als 0 und kleiner als 15 sein muss
         */
        do {
            n = IO.readInt("Geben Sie eine Zahl zwischen 1 und 15 ein: ");
        } while (n < 0 && n >= 15);

        /**
         * Im folgenden werden die ersten beiden Zeilen der Tabelle ausgegeben
         */
        IO.print("  |", 4);
        for (int i = 1; i <= n; i++) {
            IO.print(i, 4);                                         // Gibt die Werte von y < n aus
        }
        IO.println("");
        IO.print("---+", 4);
        for (int i = 1; i <= n; i++) {
            IO.print("----");                                       // Gibt die zweite Zeile aus
        }
        IO.println("");

        for (int i = 1; i <= n; i++) {
            IO.print(i + " |", 4);                                  // Gibt den aktuellen Wert für x aus
            for (int j = 1; j <= n; j++) {
                IO.print(i % j, 4);                                 // gibt den Wert für x % y aus
            }
            IO.println("");                                         // Beginnt eine neue Zeile
        }
    }

}