import AlgoTools.IO;

/**
 * Fuehrt ein Programm aus, dass Potenzen, Ackermannfunktionen und Palindrome
 * bestimmt.
 *
 * @author pstammer, chbroecker
 * @version 12.11.15.
 */
public class Rekursion {

    /**
     * Definiert die akzeptierten Befehle
     */
    private static char[] befehle = {
            'a',
            'p',
            'g',
            'e'
    };

    /**
     * Legt global fest, ob das Programm nach dem Beenden neu gestartet werden
     * soll.
     */
    public static boolean restartOnSuccess = true;

    /**
     * @param args Startparameter
     */
    public static void main(String[] args) {
        start();
    }

    /**
     * Startet das Script, damit es auch rekusiv aufgerufen werden kann und
     * leitet den Programmablauf ein
     *
     * @throws RuntimeException wenn der eingelesene Befehl ungueltig ist.
     */
    public static void start() {
        boolean gueltigeEingabe = true;
        char befehl = 'a';
        do {
            try {
                befehl = eingabeEinlesen();
                gueltigeEingabe = eingabeTesten(befehl);
                if (!gueltigeEingabe) {
                    throw new RuntimeException("Bitte geben Sie einen " +
                            "gueltigen Befehl ein:");
                }
            } catch (RuntimeException e) {
                IO.println(e);
            }

        } while (!gueltigeEingabe);

        // Fuehrt den eingegebenen Befehl aus
        try {
            executeFunction(befehl);
        } catch (RuntimeException e) {
            IO.println(e);

            char con = 'n';
            do {
                con = IO.readChar("Wollen Sie nochmal von vorne beginnen? " +
                        "y = ja, n = nein: ");
            } while (con != 'n' || con != 'y');

            if (con == 'y') {
                start();
            }
        }
    }

    /**
     * Gibt einen Promt aus und liesst einen Befehl repraesentiert durch eine
     * Zeichenkette ein.
     *
     * @return char der eingelesene Befehl
     */
    public static char eingabeEinlesen() {
        return IO.readChar("Bitte geben Sie einen Befehl ein: \n" +
                "\ta: Ackermann-Funktion \n" +
                "\tp: Potenz einer Zahl berechnen \n" +
                "\tg: Auf ein Paliodrom testen\n" +
                "\te: Programm beenden\n" +
                "-------------------------------\n" +
                "--> ");
    }

    /**
     * Testet ob die Eingabe gueltig ist
     *
     * @param eingabe eingegebener Befehl
     * @return boolean ob die Eingabe gueltig war
     */
    public static boolean eingabeTesten(char eingabe) {
        boolean gueltig = false;
        for (int i = 0; i < befehle.length; i++) {
            if (!gueltig && befehle[i] == eingabe) {
                gueltig = true;
            }
        }
        return gueltig;
    }

    /**
     * Startet die Funktion fuer den eingegebenen Befehl
     *
     * @param befehl eingelesener Befehl
     * @throws RuntimeException wenn der eingelesen Befehl ungueltig ist
     */
    public static void executeFunction(char befehl) {
        switch (befehl) {
            case 'a':
                initAckermann();
                break;
            case 'p':
                initPotenz();
                break;
            case 'g':
                initPalindrom();
                break;
            case 'e':
                IO.println("Vielen Dank, dass Sie dieses Programm genutzt " +
                        "haben!");
                break;
            default:
                throw new RuntimeException("Sie haben einen falschen Befehl " +
                        "eingegeben:");
        }
    }

    /**
     * Berechnet eine Zahl mit der Ackermann Funktion
     *
     * @param n Startwert 1
     * @param m Startwert 2
     * @return long ergebnis der Ackermann Funktion
     */
    public static long ackermann(long n, long m) {
        long returnValue = 0;
        if (n == 0) {
            returnValue = m + 1;
        } else if (m == 0) {
            returnValue = ackermann(n - 1, 1);
        } else {
            returnValue = ackermann(n - 1, ackermann(n, m - 1));
        }
        return returnValue;
    }

    /**
     * Berechnet die Potenz aus
     *
     * @param basis Basis der Potenz
     * @param exponent der Potenz
     * @return den berechneten Wert der Potenz
     */
    public static double potenz(double basis, int exponent) {
        if (exponent < 0) {
            return potenz(1 / basis, -exponent);
        } else if (exponent == 0) {
            return 1;
        } else {
            return basis * potenz(basis, exponent - 1);
        }
    }


    /**
     * Berechnet ob ein Wort ein Palindrom ist, also ob es vor- und
     * rueckwaerts gleich gelesen lautet
     *
     * @param wort das zu bestimmende Wort
     * @return ob erste und letzte Ziffer uebereinstimmen
     */
    public static boolean istPalindrom(char[] wort) {
        int start = 0,
                end = wort.length - 1;

        // Prueft auf Zeichenkette laenger als 3, ob Palindrom
        if (wort.length >= 2) {
            if (wort[start] == wort[end]) {
                char[] newWort = new char[wort.length - 2];
                for (int i = 1; i < wort.length - 1; i++) {
                    newWort[i - 1] = wort[i];
                }
                return istPalindrom(newWort);
            } else {
                return false;
            }
        } else if (wort.length == 0) {
            return true;
        } else {
            if (start == end) return true;
            else return false;
        }
    }

    /**
     * Startet die Ackermann Funktion und liesst die Werte ein
     *
     * @throws RuntimeException bei ungueltigen eingelesenen Werten
     */
    public static void initAckermann() {
        boolean korrekteEingabe = false;
        long n = 0;
        long m = 0;

        String nPromt = "Bitte geben Sie eine Zahl fuer n ein: ";
        String mPromt = "Bitte geben Sie eine Zahl fuer m ein: ";

        do {
            try {
                if (n <= 1) n = IO.readLong(nPromt);
                if (m <= 1) m = IO.readLong(mPromt);

                korrekteEingabe = n > 0 && m > 0;
                if (!korrekteEingabe) {
                    throw new RuntimeException("Sie haben einen falschen " +
                            "Wert fuer n oder m angegeben");
                }
            } catch (RuntimeException e) {
                korrekteEingabe = false;
            }
        } while (!korrekteEingabe);

        sleepAckermann(ackermann(n, m));
    }

    /**
     * Initialisiert das Potenzprogramm und liesst die Werte ein
     *
     * @throws RuntimeException bei ungueltigen eingelesenen Werten
     */
    public static void initPotenz() {
        boolean korrekteEingabe = false;
        double base = 0;
        int exponent = 0;

        String basePromt = "Bitte geben Sie eine Zahl fuer die Basis ein: ";
        String exponentPromt = "Bitte geben Sie eine Zahl fuer den " +
                "Exponenten ein: ";

        do {
            try {
                if (base <= 1) base = IO.readDouble(basePromt);
                if (exponent <= 1) exponent = IO.readInt(exponentPromt);

                // Es gibt keine Exceptions
                korrekteEingabe = true;
                if (!korrekteEingabe) {
                    throw new RuntimeException("Sie haben einen falschen " +
                            "Wert fuer die Basis oder den Exponenten " +
                            "angegeben");
                }
            } catch (RuntimeException e) {
                korrekteEingabe = false;
            }
        } while (!korrekteEingabe);

        sleepPotenz(potenz(base, exponent));
    }

    /**
     * Liesst die Werte fuer das PalindromProgramm ein und startet das Palindrom
     * Script
     *
     * @throws RuntimeException bei ungueltigen eingelesenen Werten
     */
    public static void initPalindrom() {
        boolean korrekteEingabe = false;
        char[] wort = {};

        String wortPromt = "Bitte geben Sie eine Wort ein: ";

        do {
            try {
                wort = IO.readChars(wortPromt);
                korrekteEingabe = wort.length > 0;
                if (!korrekteEingabe) {
                    throw new RuntimeException("Sie haben kein Wort " +
                            "angegeben: ");
                }
            } catch (RuntimeException e) {
                korrekteEingabe = false;
            }
        } while (!korrekteEingabe);
        sleepPalindrom(istPalindrom(wort));
    }

    /**
     * Verarbeitet den Rueckgabewert der Ackermann Funktion und gibt das
     * Ergebnis auf der Konsole aus
     *
     * @param value der auszugebene Wert
     */
    public static void sleepAckermann(long value) {
        IO.println("Das Ergebnis der Ackermann Funktion betraegt: " + value);

        if (restartOnSuccess) {
            start();
        }
    }

    /**
     * Wird ausgefuehrt, nachdem die Potenz berechnet wurde und gibt das
     * Ergbenis auf der Konsole aus
     *
     * @param value der auszugebene Wert
     */
    public static void sleepPotenz(double value) {
        IO.println("Die Potenz betraegt: " + value);
        if (restartOnSuccess) {
            start();
        }
    }

    /**
     * Wird ausgefuehrt, nachdem berechnet wurde, ob das eingegeben Wort ein
     * Palindrom ist, gibt das Ergbnis in der Konsole aus
     *
     * @param value der auszugebene Wert
     */
    public static void sleepPalindrom(boolean value) {
        if (value) {
            IO.println("Das Wort ist ein Palindrom");
        } else {
            IO.println("Das Wort ist kein Palindrom");
        }
        if (restartOnSuccess) {
            start();
        }
    }
}
