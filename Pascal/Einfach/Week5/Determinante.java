import AlgoTools.IO;

/**
 * @author pascalstammer
 * @version 12.11.15.
 */
public class Determinante {

    protected static boolean restartOnSuccess = true;

    public static void main(String[] args) {
        start();
    }

    /**
     * Initialisiert das Programm und liest die Werte ein
     */
    public static void start() {
        int zeilen = 0,
                spalten = 0;

        int[][] matrix;

        String zPromt = "Geben Sie die Zeilenanzahl der Matrix an";
        String sPromt = "Geben Sie die Spaltenanzahl an";

        boolean gueltigeEingabe = false;

        do {
            if (zeilen < 1) zeilen = IO.readInt(zPromt);
            if (spalten < 1) spalten = IO.readInt(sPromt);
            if (spalten > 0 && zeilen > 0) {
                gueltigeEingabe = true;
            } else {

                IO.println("Sie haben leider einen falschen Wert für die " +
                        "Zeilen oder Spaltenanzahl angegeben. Versuchen " +
                        "Sie es nochmal");
                gueltigeEingabe = false;
            }

        } while (!gueltigeEingabe);

        matrix = new int[zeilen][spalten];

        for (int i = 0; i < zeilen; i++) {
            try {
                matrix[i] = matrixEinlesen("Bitte geben Sie die Zeichen der" +
                        i + "Zeile durch ein Leerzeichen getrennt an: ",
                        spalten);
            } catch (RuntimeException e) {
                IO.println(e);
                matrix[i] = matrixEinlesen("Bitte geben Sie die Zeichen der" +
                        i + "Zeile durch ein Leerzeichen getrennt an: ",
                        spalten);
            }
        }
        sleepDeterminante(determinante(matrix));

    }

    /**
     * @param promt
     * @param spalten
     * @return die eingelesene Zeilenspalte
     */
    public static int[] matrixEinlesen(String promt, int spalten) {

        int[] matrix = IO.readInts(promt);

        if (matrix.length == spalten) {
            return matrix;
        } else {
            throw new RuntimeException("Ihre eingegebene Spalte hatte die " +
                    "falsche Anzahl an Zeichen");
        }
    }

    public static int determinante(int[][] matrix) {
        int faktor = -1,
                spalten = matrix[0].length,
                zeilen = matrix.length,
                det = 0;

        int[][] redMatrix = new int[zeilen - 1][spalten - 1];
        int spaltenBuffer = 0;
        if (zeilen == 1) {
            return matrix[0][0];
        } else {
            // Faktor berechnen
            for (int j = 0; j < spalten; j++) {
                faktor = (j % 2 == 0) ? 1 : -1;

                // Reduziere die aktuelle Matrix um die erste Reihe
                // und die j Spalte
                for (int k = 1; k < matrix.length; k++) {
                    for (int z = 0; z < redMatrix[0].length; z++) {
                        spaltenBuffer = (z >= j) ? z + 1 : z;
                        redMatrix[k - 1][z] = matrix[k][spaltenBuffer];
                    }
                }
                det += faktor * matrix[0][j] * determinante(redMatrix);
            }
            return det;
        }
    }

    /**
     * Gibt das Ergebnis in der Konsole aus
     *
     * @param value der berechnete Wert, der ausgegeben Wird
     */
    public static void sleepDeterminante(int value) {
        IO.println("Die Determinante der Matrix beträgt: " + value);

        if (restartOnSuccess) {
            sleep();
        }
    }

    public static void sleep() {
        boolean gueltigeEingabe = false;
        do {
            char befehl = IO.readChar("Wollen Sie das Programm nochmal " +
                    "Starten? y = ja, n = nein");
            if (befehl == 'y' || befehl == 'n') {
                gueltigeEingabe = true;
            } else {
                gueltigeEingabe = false;
                IO.println("Sie haben leider einen falschen Befehl angegeben." +
                        "Versuchen Sie es nochmal");
            }

        } while (!gueltigeEingabe);
    }
}
