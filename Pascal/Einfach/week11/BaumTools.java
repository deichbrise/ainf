/******************************  BaumTools.java  ******************************/

import AlgoTools.IO;

/**
 * Utility-Klasse mit einigen Algorithemn fuer Baeume
 */
public class BaumTools {

  /**
   * Prüft ob ein Teilbaum vollständig ist, also ob Ebenen immer
   * vollständig besetzt sind
   * @param b der zu ueberprüfende Baum
   * @return ist Teilbaum vollständig
   */
  public static boolean istVollstaendig(Baum b) {

    // Am Ende angekommen
    if(b.empty()) {
      return true;
    }

    // Schaut, ob beide Teilbäume die gleiche Tiefe besitzen
    // Wenn ja, dann checke die Teilbäume auf Vollständigkeit
    if(baumTiefe(b.left()) != baumTiefe(b.right())) {
      return false;
    } else {
      return istVollstaendig(b.left()) && istVollstaendig(b.right());
    }
  }

  /**
   * Gibt die Anzahl an Knoten rekursiv aus
   * @param b den zu ueberprüfenden Baum
   * @return die anzahl der Knoten
   */
  public static int anzahlKnoten(Baum b) {
    if(b.empty())
      return 0;

    return 1 + anzahlKnoten(b.left()) + anzahlKnoten(b.right());
  }

  /**
   * Erstellt rekursiv einen Baum nach einer inorder + postorder
   * Bauvorschrift
   * @param inorder inorder regel
   * @param postorder postorder regel
   * @return den erstellten Baum
   */
  public static VerweisBaum inorderPostorderBau(int[] inorder, int[]
          postorder) {

    if(inorder.length == 0 || postorder.length == 0)
      return new VerweisBaum();

    int root = postorder[postorder.length - 1];

    int index = 0;
    while(inorder[index] != root && index < inorder.length) {
      index++;
    }

    if(inorder[index] != root) {
      throw new RuntimeException("Error: No valid inorder or postorder" +
              "arguments");
    }

    // Split inorder and Postorder
    int[] leftPostorder = new int[index];
    int[] rightPostorder = new int[postorder.length - index - 1];
    int[] leftInorder = new int[index];
    int[] rightInorder = new int[inorder.length - index - 1];

    //Kopiere linken Teilbaum
    for(int j = 0; j < index; j++) {
      leftInorder[j] = inorder[j];
      leftPostorder[j] = postorder[j];
    }
    //Kopiere rechten Teilbaum
    for(int j = index + 1; j < inorder.length; j++) {
      rightInorder[j - (index + 1)] = inorder[j];
      rightPostorder[j - (index + 1)] = postorder[index + (j - (index + 1))];
    }

    return new VerweisBaum(inorderPostorderBau(leftInorder, leftPostorder), root, inorderPostorderBau(rightInorder, rightPostorder));

  }

  /**
   * Berechnet rekursiv die Baumtiefe
   * @param b den zu berechnende Baum
   * @return die Baumtiefe
   */
  public static int baumTiefe(Baum b) {
    if(b.empty())
      return 0;
    else {
      int links = baumTiefe(b.left());
      int rechts = baumTiefe(b.right());
      //Liefere das Maximum
      if (links >= rechts) {
        return 1 + links;
      } else {
        return 1 + rechts;
      }
    }
  }

  /**
   * Druckt einen Baum auf der Konsole ebenenweise aus.
   * Nutzt baumTiefe(Baum), printEbene(Baum,int,int) und spaces(int).
   * @param b der zu druckende Baum
   */
  public static void printBaum(Baum b) {

    //Berechne die Tiefe des Baumes
    int resttiefe = baumTiefe(b);

    //Gehe die Ebenen des Baumes durch
    for(int i = 0; i < resttiefe; i++) {
      //Drucke die Ebene, beruecksichtige Anzahl der Leerzeichen
      //fuer korrekte Einrueckung
      printEbene(b, i, spaces(resttiefe - i));
      IO.println();
      IO.println();
    }

  }

  /**
   * Druckt eine Ebene eines Baumes aehnlich der Breitensuche
   * aus. 0 ist dabei die Wurzel. Vor und nach jedem Element
   * werden spaces Leerzeichen eingefuegt.
   * @param b der auszugebende Baum
   * @param ebene die gewuenschte Ebene beginnend bei 0
   * @param spaces Anzahl von Leerzeichen vor und nach jedem Element
   */
  public static void printEbene(Baum b, int ebene, int spaces) {

    //Wenn 0 erreicht, drucke Element mit Leerzeichen
    if(ebene == 0) {

      IO.print("", spaces);
      if(b != null) {
        IO.print(b.value());
      }
      else { //Wenn Nullzeiger uebergeben wurde, ein Leerzeichen drucken
        IO.print(" ");
      }
      IO.print(" ", spaces + 1);

    } else {

      //Steige rekursiv ab, betrachte Soehne als Teilbaeume und verringere
      //dabei die zu druckende Ebene. In endende Aeste wird mit dem Nullzeiger
      //gelaufen.
      if(b != null && !b.left().empty()) {
        printEbene(b.left(), ebene - 1, spaces);
      }
      else {
        printEbene(null, ebene - 1, spaces);
      }

      if(b != null && !b.right().empty()) {
        printEbene(b.right(), ebene - 1, spaces);
      }
      else {
        printEbene(null, ebene - 1, spaces);
      }

    }

  }

  /**
   * Berechnet die Anzahl an benoetigten Leerzeichen fuer
   * eine korrekte Einrueckung, also 0.5 * (2^(ebene) - 2).
   * @param ebene die Ebene, Blaetter sind Ebene 1, darueber aufsteigend
   * @return Anzahl der Leerzeichen zur Elementtrennung
   */
  private static int spaces(int ebene) {

    if(ebene == 1) {
      return 0;
    }
    else {
      //verdoppele die Leerzeichen gegenueber der Ebene darunter
      //und fuege ein weiteres Leerzeichen hinzu
      return 2 * spaces(ebene - 1) + 1;
    }

  }

}
