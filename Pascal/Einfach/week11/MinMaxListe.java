/******************************* MinMaxListe.java *****************************/
import AlgoTools.IO;
import com.deichbrise.week10.Liste;
import com.deichbrise.week10.VerweisListe;


/**
 * Erweitert die VerweisListe um die Funktionalität stets Zugriff
 * auf das größte und kleinste Element zu haben. Dabei wird davon ausgegangen,
 * dass nur Objekte eingefügt werden, die Comparable implementieren.
 */
public class MinMaxListe extends VerweisListe {

  private Eintrag min;
  private Eintrag max;

  public MinMaxListe() {
    min = new Eintrag();
    max = new Eintrag();
  }

  public MinMaxListe(Liste liste) {
    min = new Eintrag();
    max = new Eintrag();

    liste.reset();

    while(!liste.endpos()) {
      this.insert(liste.elem());
      liste.advance();
    }

  }

  /**
   * Fügt ein Element ein und setzt das maximale und minimale Element
   * @param x das hinzuzufügende Elemen
   */
  @Override
  public void insert(Object x) {
    Comparable newElement = (Comparable) x;
    super.insert(newElement);

    if(min.inhalt == null) {
      min.inhalt = newElement;
      max.inhalt = newElement;
    }

    if(newElement.compareTo(min.inhalt) < 0 ) {
      min.inhalt = x;
    } else if(newElement.compareTo(max.inhalt) > 0) {
      max.inhalt = x;
    }
  }

  /**
   * Löscht ein Element und berechnet das größte und kleinste
   * Element neu
   */
  @Override
  public void delete() {
    super.delete();
    MinMaxListe temp = new MinMaxListe(this);
    min.inhalt = temp.getMin();
    max.inhalt = temp.getMax();
  }

  /**
   * Gibt das kleinste Element der Liste zurück
   *
   * @return Das kleinste Element
   */
  public Object getMin() {
    return this.min.inhalt;
  }

  /**
   * Gibt das größte Element der Liste zurück
   *
   * @return Das größte Element
   */
  public Object getMax() {
    return this.max.inhalt;
  }
}
