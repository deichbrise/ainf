import AlgoTools.IO;
/**
 * Model Book
 *
 * @author pstammer
 * @version 07.12.15.
 */
public class Buch {

    /**
     * Title des Buches
     */
    protected char[] title;

    /**
     * Autor des Buches
     */
    protected char[] author;

    /**
     * Erscheinungsdatum des Buches
     */
    protected int date;

    /**
     *
     * @param title
     * @param author
     * @param date
     */
    public Buch(char[] title, char[] author, int date) {
        this.setTitle(title);
        this.setAuthor(author);
        this.setDate(date);
    }

    /**
     *
     * @return den title des Buches
     */
    public char[] getTitle() {
        return title;
    }

    /**
     *
     * @param title des Buches
     */
    public void setTitle(char[] title) {
        this.title = title;
    }

    /**
     *
     * @return den Autornamen des Buches
     */
    public char[] getAuthor() {
        return author;
    }

    /**
     *
     * @param author Autornamen des neuen Buches
     */
    public void setAuthor(char[] author) {
        this.author = author;
    }

    /**
     *
     * @return das Datum des Buches
     */
    public int getDate() {
        return date;
    }

    /**
     *
     * @param date Datum des neuen Buches
     */
    public void setDate(int date) {

        if(date <= 0 || date >= 2012) throw new RuntimeException("Date out of" +
                "Range");
        this.date = date;
    }

    /**
     * Gibt die Buchdaten auf der Konsole aus
     */
    public void printBook() {
        IO.println("Titel: ");
        for(int j=0; j<this.title.length; j++){
            IO.print(this.title[j]);
        }
        IO.println();
        IO.println("Autor: ");
        for(int j=0; j<this.author.length; j++){
            IO.print(this.author[j]);
        }
        IO.println();
        IO.println("Erscheinungsjahr: ");

        IO.print(this.date);

        IO.println();
    }
}
