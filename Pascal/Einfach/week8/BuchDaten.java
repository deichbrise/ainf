/******************************  BuchDaten.java  ****************************/

import AlgoTools.IO;

/**
 * Einfacher Algorithmus zur Erfassung von Buchdaten mit Hilfe von
 * char-Arrays
 */

public class BuchDaten{

  /**
  * Erzeugt Buchdaten-Array aus uebergebenen Attributen und
  * haengt diese der als char[][][] Array uebergebenen Liste an.
  *
  * @param titel Vorname des anzulegenden Buchdatums
  * @param autor Nachname des anzulegenden Buchdatums
  * @param erscheinungsjahr Alter des anzulegenden Buchdatums
  * @param liste Array mit bereits vorhandenen Buchdaten
  * @return Kopie der Buchdatenliste ergaenzt um das neue Datum
  * @throws RuntimeException falls erscheinungsjahr kleiner 0 oder groesser 2012
  */
  public static char[][][] addBuch(char[] titel, char[] autor,
                                   int erscheinungsjahr, char[][][] liste){

    if(erscheinungsjahr <= 0 || erscheinungsjahr > 2012)
      throw new RuntimeException("Falsches Erscheinungsjahr");

    // Neues Buch anlegen
    char[][] neuesBuch = new char[3][];
    neuesBuch[0] = new char[titel.length];
    neuesBuch[1] = new char[autor.length];
    neuesBuch[2] = new char[4];

    // Titel und Autor kopieren
    for(int i = 0; i < titel.length; i++) neuesBuch[0][i] = titel[i];
    for(int i = 0; i < autor.length; i++) neuesBuch[1][i] = autor[i];

    // Erscheinungsdatum casten
    neuesBuch[2][0]=(char)(erscheinungsjahr/1000 + '0');
    erscheinungsjahr=erscheinungsjahr%1000;
    neuesBuch[2][1]=(char)(erscheinungsjahr/100 + '0');
    erscheinungsjahr=erscheinungsjahr%100;
    neuesBuch[2][2]=(char)(erscheinungsjahr/10 + '0');
    erscheinungsjahr=erscheinungsjahr%10;
    neuesBuch[2][3]=(char)(erscheinungsjahr + '0');


    // Neue Liste Anlegen und kopieren
    char[][][] tempList = new char[liste.length + 1][][];
    for(int i = 0; i < liste.length; i++) tempList[i] = liste[i];

    // Neues Buch hinzufügen
    tempList[liste.length] = neuesBuch;

    return tempList;

  }

  /**
   * Erzeugt Buchdaten-Array aus uebergebenen Attributen und
   * haengt diese der als Buch[] Array uebergebenen Liste an.
   *
   * @param titel Vorname des anzulegenden Buchdatums
   * @param autor Nachname des anzulegenden Buchdatums
   * @param erscheinungsjahr Alter des anzulegenden Buchdatums
   * @param liste Array mit bereits vorhandenen Buchdaten
   * @return Kopie der Buchdatenliste ergaenzt um das neue Datum
   *
   * @TODO Noch ein Buchrepository hinzufügen
   */
  public static Buch[] addBuchOpt(char[] titel, char[] autor,
                                   int erscheinungsjahr, Buch[] liste) {

    Buch[] tempListe = new Buch[liste.length + 1];
    for(int i = 0; i < liste.length; i++) {
      tempListe[i] = liste[i];
    }

    try {
      Buch neuesBuch = new Buch(titel, autor, erscheinungsjahr);
      tempListe[liste.length] = neuesBuch;
    } catch(RuntimeException e) {
      IO.println(e);
    }



    return tempListe;
  }

  /**
  * Liesst iteriert Buchendaten ein und gibt die Daten anschliessend
  * auf der Konsole aus.
  */
  public static void main(String[] argv) {

    // Diese Methode soll NICHT geändert werden.

    char[][][] liste = new char[0][][];

    do{

      IO.println("Bitte geben Sie Buchdaten ein.");
      char[] titel=IO.readChars("Bitte den Titel eingeben: ");
      char[] autor=IO.readChars("Bitte den Autor eingeben: ");
      int erscheinungsjahr=IO.readInt("Bitte das Erscheinungsjahr eingeben: ");

      liste=addBuch(titel,autor,erscheinungsjahr,liste);

    }while(IO.readInt("Moechten Sie weitere Buecher eingeben?"
                          +" Abbruch mit 0: ")!=0);


    for(int i=0; i<liste.length; i++){
      IO.println("Buch Nr. "+(i+1));
      IO.println("Titel: ");
      for(int j=0; j<liste[i][0].length; j++){
        IO.print(liste[i][0][j]);
      }
      IO.println();
      IO.println("Autor: ");
      for(int j=0; j<liste[i][1].length; j++){
        IO.print(liste[i][1][j]);
      }
      IO.println();
      IO.println("Erscheinungsjahr: ");
      for(int j=0; j<liste[i][2].length; j++){
        IO.print(liste[i][2][j]);
      }
      IO.println();
    }
  }
}
