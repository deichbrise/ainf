
/**
 * Sortiert eine Liste von fünfstelligen Zahlen mit dem ElfSortAlgorithmus
 *
 * @author pstammer, chbroecker
 * @version 06.12.15.
 */
public class ElfSort {

    /**
     *
     * @param pakete
     * @param ziffer
     * @return
     */
    public static int[] sort(int[] pakete, int ziffer) {
        if( pakete.length <= 1 && ziffer < 0) {
            return pakete;
        }

        // Lege neues Fach an

        int[][] fach = {{},{},{},{},{},{},{},{},{},{}};

        // Durchlaufe jedes Paket
        for (int i = 0; i < pakete.length; i++) {
            int paketnummer = pakete[i];

            for(int k = 0; k < ziffer; k++) {
                paketnummer /= 10;
            }

            paketnummer = paketnummer % 10;

            fach[paketnummer] = legeInFach(fach[paketnummer], pakete[i]);
        }

        // Sortiert die Fächer
        for (int i = 0; i < fach.length; i++) {
            fach[i] = sort(fach[i], ziffer - 1);
        }

        int[] returnArray = new int[pakete.length];
        int index = 0;

        // Kopiert die Fächer zu einem Array zusammen
        for(int fachNummer = 0; fachNummer < fach.length; fachNummer++) {
            for (int i = 0; i < fach[fachNummer].length; i++) {
                returnArray[index++] = fach[fachNummer][i];
            }
        }
        return returnArray;
    }

    /**
     *
     * @param ablagefach
     * @param paketnr
     * @return
     */
    public static int[] legeInFach(int[] ablagefach, int paketnr) {

        // Erstelle ein bufferArray, dass um 1 größer als das alte Ablagefach ist
        int[] bufferArray = new int[ablagefach.length + 1];

        // Kopiere die Werte das Arrays
        for (int i = 0; i < ablagefach.length; i++) {
            bufferArray[i] = ablagefach[i];
        }

        // Fuege das letzte Paket an
        bufferArray[bufferArray.length - 1] = paketnr;


        return bufferArray;
    }
}
