import AlgoTools.IO;

/**
 * @author pstammer, chbroecker
 *
 * @version 29.10.2015
 *
 * Diese Klasse wandelt eine Float Zahl in Binärschreibweise um.
 *
 * Zuerst wird der größtmögliche Expontent bestimmt, dann die Mantisse
 * mit mantisse = zahl / 2^Exponent
 *
 * Danach wird die Binärschreibweise des Exponenten errechnet, allerdings in umgekehrer Reihenfolge
 * sodass die Zeichen zwischengespeichert werden müssen, dann umgedreht werden und dann erst ausgegeben
 * werden
 *
 * Danach wird die Binärschreibweise der Mantisse ausgegeben.
 */
public class Float {

    public static void main(String[] args) {

        /**
         * @var int expontentSize
         * @var int mantisseSize
         * @var int exponent
         */
        int     exponentSize = 8,
                mantisseSize = 23,
                exponent = 0,
                teiler = 1,
                verschiebefaktor = 0,
                exponentBuffer,
                universellerZaehler = 0;

        /**
         * @var byte expontentDigit1
         * @var byte expontentDigit2
         * @var byte expontentDigit3
         * @var byte expontentDigit4
         * @var byte expontentDigit5
         * @var byte expontentDigit6
         * @var byte expontentDigit7
         *
         * Da wir noch keine Arrays nutzen dürfen, müssen die einzelnen Werte
         * in einfachen Variablen gespeichert werden, damit sie nachher in
         * umgekehrter Reihenfolge ausgegeben werden können.
         *
         * Wenn Arrays benutzer werden dürfen, dann würde der Code folgender
         * Maßen aussehen
         *
         * byte expontentDigits[] = new Byte[expontentSize]
         */
        byte    expontentDigit1 = 0,
                expontentDigit2 = 0,
                expontentDigit3 = 0,
                expontentDigit4 = 0,
                expontentDigit5 = 0,
                expontentDigit6 = 0,
                expontentDigit7 = 0,
                expontentDigit8 = 0;

        /**
         * @var float zahl
         *
         * Ließt eine Fließkommazahl ein, für die die binärschreibweise
         * ausgegeben werden soll die zahl darf nicht zwischen -1 und 1 liegen
         */
        float zahl;
        do {
            zahl = IO.readFloat("Bitte geben Sie eine Zahl ein: ");
        } while ((zahl < 1 && zahl > 0) || (zahl > -1 && zahl < 0));


        /**
         * @var floar mantisse
         */
        float mantisse;
        /**
         * Wertet aus, ob die Zahl negativ oder positiv ist
         * und gibt dementsprechend den bit aus
         *
         * Außerdem wird noch ein Trennzeichen gedruckt, damit die
         * einzelnen Bestandteile der Codierung besser sichtbar werden
         */
        if(zahl >= 0) IO.print(0 + "|");
        else {
            IO.print(1 + "|");
            zahl *= -1.0;
        }




        /**
         * Bestimmt den größtmöglichen Exponenten zur Basis 2
         */
        for (int i = 2; i <= zahl; i *= 2) {
            exponent++;
            teiler = i;
        }

        /**
         * Berechnet die Mantisse
         */
        mantisse = zahl / teiler;



        /**
         * Ausnahmehandlung für einen Expontent = 0
         */
        if(exponent == 0) {
            for(int i = 0; i < exponentSize; i++) {
                IO.print(0);
            }
        } else {

            /**
             * @var byte bufferDigit
             *
             * speichert den Wert für einen durchlauf zwischen, damit die
             * richtige Variable befüllt werden kann
             *
             * Die Variable wird vorher erzeugt, damit die Laufzeit verringert
             * wird.
             */
            byte  bufferDigit;

            universellerZaehler = 0;

            /**
             * Neue Methode um die Zahlen richtig auszugeben...
             *
             * int bufferExpo = exponent * 64;
             * while(bufferExpo != 0) {
             * if(bufferExpo % 2 == 0) {
             * IO.print(0);
             * } else {
             * IO.print(1);
             *
             * }
             *  bufferExpo /= 2;
             * }
             * IO.print("|");
             *
             * @TODO: Funktioniert nicht
             *
             */



            /**
             * Umwandlung des Exponenten in Binärschreibweise
             *
             * Wenn der Expontent gerade ist, dann drucke eine Null,
             * wenn er ungerade ist, drucke eine 1 und verringere um 1
             *
             * Danach halbiere die Zahl
             *
             * Solange expontent != 0 ist
             *
             * @throws RuntimeException
             */
            while(exponent != 0) {
                if(exponent % 2 == 0) {
                    bufferDigit = 0;
                } else {
                    bufferDigit = 1;
                }
                switch(universellerZaehler) {
                    case 0: expontentDigit1 = bufferDigit; break;
                    case 1: expontentDigit2 = bufferDigit; break;
                    case 2: expontentDigit3 = bufferDigit; break;
                    case 3: expontentDigit4 = bufferDigit; break;
                    case 4: expontentDigit5 = bufferDigit; break;
                    case 5: expontentDigit6 = bufferDigit; break;
                    case 6: expontentDigit7 = bufferDigit; break;
                    default:
                        throw new RuntimeException("Der Expontent weist einen " +
                                "zu großen Wertebereich auf..");
                }
                exponent /= 2;
                universellerZaehler++;

            }

            // universellen Zaehler zurücksetzen
            universellerZaehler = 0;

            // Ausgabe des Expontent in Binärschreibweise
            IO.print(""
                    + expontentDigit8
                    + expontentDigit7
                    + expontentDigit6
                    + expontentDigit5
                    + expontentDigit4
                    + expontentDigit3
                    + expontentDigit2
                    + expontentDigit1);
        }

        // Drucke ein Trennzeichen um den Expontenten von der Mantisse zu trennen
        IO.print("|");

        /**
         * Der Algoritmus um die Mantisse binär darzustellen
         *
         * Zuerst schaue, ob die Mantisse größer als 1 ist, wenn ja,
         * dann reduziere sie um 1.0, danach multipliziere sie mit 2
         * und schaue, ob das ergebnis größer als 1 ist, wenn ja, drucke eine 1,
         * wenn nicht, dann drucke eine 0, und das 23 mal
         */
        for(int z = 0; z < mantisseSize; z++) {
            if(mantisse >= 1.0) mantisse -= 1.0;
            mantisse *= 2.0;
            if(mantisse >= 1.0) IO.print(1);
            else IO.print(0);
        }

        // Obligatorischer Zeilenumbruch
        IO.println();
    }
}
