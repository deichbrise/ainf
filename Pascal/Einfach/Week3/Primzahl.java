import AlgoTools.IO;

/**
 * @author pstammer, chbroecker
 *
 * @version 28.10.15.
 *
 * Diese Klasse nimmt eine Zahl entgegen und testet, ob diese Zahl eine Primzahl
 * ist
 */
public class Primzahl {

    public static void main(String[] args) {
        /**
         * @var int primeNumber
         * @var int j
         * @var boolean isPrimeNumber
         */
        int     primeNumber,
                j = 1;
        boolean isPrimeNumber = true;

        /**
         * Lies eine Zahl ein, die größer als 1 ist
         */
        do {
            primeNumber = IO.readInt("Geben Sie eine Zahl ein: ");
        } while (primeNumber < 1);

        /**
         * stellt fest, ob die eingegeben Zahl eine Primzahl ist,
         *
         * testet, ob die Zahl gerade ist, wenn ja, ist es keine Primzahl
         *
         * wenn ungerade, dann teste jeden ungerade Teiler von 1 bis sqr(Zahl)
         * durch,
         *
         * denn jeder gerade Teiler ist ein vielfaches von 2 und dann wäre die
         * Zahl wieder gerade..
         */
        if (primeNumber % 2 != 0) {
            while (j < primeNumber && isPrimeNumber && (j * j) <= primeNumber )
            {
                if(primeNumber % j == 0 && j != 1 ) isPrimeNumber = false;
                j = j + 2;
            }
        } else {
            isPrimeNumber = false;
        }

        // Anzahl der Schritte drucken
        IO.println("Anzahl der Schritte: " + j);

        // Ausnahmen behandeln
        switch(primeNumber) {
            case 0:case 1: isPrimeNumber = false; break;
            case 2: isPrimeNumber = true; break;
            default:
                isPrimeNumber = isPrimeNumber;
        }

        // Ergebnis drucken
        if(isPrimeNumber) IO.println("Ist Primzahl");
        else IO.println("Ist keine Primzahl");

    }
}
