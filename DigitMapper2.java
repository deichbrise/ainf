
import AlgoTools.IO;

/**
 * @version 26.10.2015
 *
 * @author pstammer <pstammer@uos.de>
 *
 * Diese Klasse liest eine Zahl zwischen 10 und 999 ein und gibt diese Zahl in englischer Sprache aus
 */
public class DigitMapper2 {

    public static void main(String[] args) {
        int zahl;

        do{
            zahl = IO.readInt("Bitte geben Sie eine Zahl zwischen 10 und 999 ein: ");
        } while (zahl < 10 || zahl > 999);

        /**
         * Gebe die 100 aus
         */

        switch ((zahl % 1000) / 100) {
            case 0: break;
            case 1: IO.print("one hundred ");
                break;
            case 2: IO.print("two hundred ");
                break;
            case 3: IO.print("three hundred ");
                break;
            case 4: IO.print("four hundred ");
                break;
            case 5: IO.print("five hundred ");
                break;
            case 6: IO.print("six hundred ");
                break;
            case 7: IO.print("seven hundred ");
                break;
            case 8: IO.print("eight hundred ");
                break;
            case 9: IO.print("nine hundred ");
                break;
            default: IO.print("Fehler bei den Hundertern ");

        }
        if(zahl % 100 > 0) {
            IO.print("and ");
        }
        if((zahl % 100) > 10 && (zahl % 100) < 20) {
            switch(zahl % 100) {
                case 11: IO.print("eleven");
                    break;
                case 12: IO.print("twelve");
                    break;
                case 13: IO.print("thirtheen");
                    break;
                case 14: IO.print("fourteen");
                    break;
                case 15: IO.print("fivteen");
                    break;
                case 16: IO.print("sixteen");
                    break;
                case 17: IO.print("seventeen");
                    break;
                case 18: IO.print("eighteen");
                    break;
                case 19: IO.print("nineteen");
                    break;
                default:
                    IO.print("Fehler bei den Ausnahmen zwischen 10 und 20");
            }
        } else {
            switch((zahl % 100) / 10) {
                case 0: break;
                case 2: IO.print("twenty-");
                    break;
                case 3: IO.print("thirty-");
                    break;
                case 4: IO.print("fourty-");
                    break;
                case 5: IO.print("fivty-");
                    break;
                case 6: IO.print("sixty-");
                    break;
                case 7: IO.print("seventy-");
                    break;
                case 8: IO.print("eighty-");
                    break;
                case 9: IO.print("ninety-");
                    break;
                default: IO.print("Fehler bei den Zehnern");
            }

            switch(zahl % 10) {
                case 0:
                    break;
                case 1: IO.print("one");
                    break;
                case 2: IO.print("two");
                    break;
                case 3: IO.print("three");
                    break;
                case 4: IO.print("four");
                    break;
                case 5: IO.print("five");
                    break;
                case 6: IO.print("six");
                    break;
                case 7: IO.print("seven");
                    break;
                case 8: IO.print("eight");
                    break;
                case 9: IO.print("nine");
                    break;
                default: IO.print("Fehler bei den Einern");
            }
        }
        IO.println("");
    }


}