#  Readme

## Git Commands

Wenn neue Datei in den Ordnern angelegt wurde, muss diese Datei händisch zum git Repository hinzugefügt werden:

	git add *  bzw. git add 'path'

Danach muss ein Commit gemacht werden per:

	git commit -am "Commit-Message"

Danach muss das lokale Repository mit dem Remote-Repository gesyncd werden per:

	git push