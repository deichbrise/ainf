/*****************************
 * Collatz.java
 *********************************/

import AlgoTools.IO;

/**  Berechnet Collatz-Funktion, d.h.                     
 *   Anzahl der Iterationen der Funktion g: N -> N        
 *   bis die Eingabe auf 1 transformiert ist              
 *   mit g(x) = x/2 falls x gerade, 3*x+1 sonst          
 */

public class Collatz {

    public static void main(String[] argv) {
        for (int i = 1; i <= 25; i++) {
            collatz(i);
        }
    }

    public static void collatz(int x) {
        int z, a;
        a = x;                                      // definiere 2 Variablen
        z = 0;                                      // setze z auf blabla0

        while (x != 1) {                            // solange x ungleich 1 ist
            x = x % 2 == 0 ? x / 2 : x * 3 + 1;     // verdreifache x und add. 1
            z++;                                    // erhoehe z um eins
        }
        IO.println("Anzahl der Iterationen(" + a + "): " + z);             // gib z aus

    }
}
